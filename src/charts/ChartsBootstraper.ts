import {FrameworkTimelineChart} from './FrameworkTimelineChart';

export class ChartsBootstraper {

  constructor(){

  }

  public static initialize(){
    const frameworkTimeline = FrameworkTimelineChart.initialize();
    return {frameworkTimeline};
  }
}
