import * as Chart from 'chart.js';
import {data} from "../data/DataRegistry";

const Colors = [
    '#c62828',
    '#ad1457',
    '#6a1b9a',
    '#4527a0',
    '#283593',
    '#1565c0',
    '#0277bd',

    '#00838f',
    '#00695c',
    '#2e7d32',
    '#558b2f',
    '#9e9d24',

    '#f9a825',
    '#ff8f00',
    '#ef6c00',
    '#d84315',
    '#4e342e',
    '#424242',
    '#37474f',
];


const CHART_BASE_CONFIG = {
    type: 'line',
    data: {
        datasets: []
    },
    options: {
        responsive: true,
        title: {
            display: true,
            text: "stackoveflow questions over time"
        },
        scales: {
            xAxes: [{
                type: "time",
                display: true,
                scaleLabel: {
                    display: true,
                    labelString: 'Date'
                },
                ticks: {
                    major: {
                        fontStyle: "bold",
                        fontColor: "rgba(255, 255, 255, 0.8)"
                    }
                }
            }],
            yAxes: [{
                display: true,
                scaleLabel: {
                    display: false,
                    labelString: 'value'
                }
            }]
        }
    }
};

type DataSet = Array<any>;

interface ChartPreparation {
    chart: Chart;
    chartConfig: any;
    dataSets: DataSet;
}

export class FrameworkTimelineChart {

    constructor() {

    }

    public static getData() {
        return data['web-ui-frameworks-history'];
    }

    public static initialize() {
        const ctx = document.getElementById('framework-timeline-tooltip');
        const data = FrameworkTimelineChart.getData();
        const config = Object.assign({}, CHART_BASE_CONFIG);
        const datasets = data.map((item, idx) => {
            return {
                label: item.label,
                backgroundColor: Colors[idx % Colors.length],
                borderColor: Colors[idx % Colors.length],
                fill: false,

                data: item.points,
                pointRadius: 0,
                lineTension: 1,
                borderWidth: 2
            }
        });
        return {chart: new Chart(ctx, config), chartConfig: config, datasets: datasets};
    }
}
