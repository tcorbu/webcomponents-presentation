export declare class ChartsBootstraper {
    constructor();
    static initialize(): {
        frameworkTimeline: {
            chart: any;
            chartConfig: {
                type: string;
                data: {
                    datasets: any[];
                };
                options: {
                    responsive: boolean;
                    title: {
                        display: boolean;
                        text: string;
                    };
                    scales: {
                        xAxes: {
                            type: string;
                            display: boolean;
                            scaleLabel: {
                                display: boolean;
                                labelString: string;
                            };
                            ticks: {
                                major: {
                                    fontStyle: string;
                                    fontColor: string;
                                };
                            };
                        }[];
                        yAxes: {
                            display: boolean;
                            scaleLabel: {
                                display: boolean;
                                labelString: string;
                            };
                        }[];
                    };
                };
            };
            datasets: any;
        };
    };
}
