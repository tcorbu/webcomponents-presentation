import './data/DataRegistry';
import * as Reveal from 'reveal.js';
import {RevealBootstraper} from './reveal/RevealBootstraper';
import {ChartsBootstraper} from "./charts/ChartsBootstraper";

let DataMap = {};
let timerIdHolder = {id: null};

const clearProgressTimer = function () {
    if (timerIdHolder.id) {
        window.clearTimeout(timerIdHolder.id);
        timerIdHolder.id = null;
    }
};

const scheduleAction = function (action, milisToWait) {
    clearProgressTimer();
    timerIdHolder.id = setTimeout(() => {
        timerIdHolder.id = null;
        action()
    }, milisToWait);
};

function startSlideAnimation(slideId, slideElement) {


    console.log(slideId);
    if (!slideId) {
        return;
    }
    const data = DataMap['frameworks-timeline'];

    if (slideId === 'frameworks-timeline') {
        const datasetsCopy = JSON.parse(JSON.stringify(data.datasets));
        const datasets = datasetsCopy.filter((item) => {
            return item.label !== 'javascript' && item.label !== 'jquery';
        }).map((item) => {
            item.scheduledData = item.data;
            item.data = [];
            item.data.push(item.scheduledData.shift());
            return item;
        });
        console.log(datasets);
        data.chartConfig.data.datasets = datasets;
        data.chart.update();

        let currentIndex = 0;
        const count = datasets[0].scheduledData.length;

        const done = function () {
            scheduleAction(() => {
                const jqueryDataset = datasetsCopy.find((item) => {
                    return item.label === 'jquery';
                });
                console.log(jqueryDataset);
                data.chartConfig.data.datasets.push(jqueryDataset);
                data.chart.update();
                scheduleAction(() => {
                    const jqueryDataset = datasetsCopy.find((item) => {
                        return item.label === 'javascript';
                    });
                    data.chartConfig.data.datasets.push(jqueryDataset);
                    data.chart.update();
                }, 2000);
            }, 2000);


        };

        const pushData = function () {
            if (currentIndex >= count) {
                done();
                return;
            }
            for (const dataset of datasets) {
                const item = dataset.scheduledData.shift();

                if (item.y !== 0) {
                    dataset.firstValueInSeries = true;
                }
                if (dataset.firstValueInSeries) {
                    dataset.data.push(item);
                }
            }
            ;
            data.chart.update();
            currentIndex++;
            scheduleAction(pushData, 100);
        };
        scheduleAction(pushData, 100);
    }

}

const done = ChartsBootstraper.initialize();
DataMap['frameworks-timeline'] = done.frameworkTimeline;
Reveal.addEventListener( 'ready', function( event ) {
	const slideId = event.currentSlide.dataset.slideId;
    startSlideAnimation(slideId, DataMap[slideId]);
} );
Reveal.addEventListener('slidechanged', function (event) {
    const slideId = event.currentSlide.dataset.slideId;
    clearProgressTimer();
    startSlideAnimation(slideId, event.currentSlide);
});

RevealBootstraper.initialize();
