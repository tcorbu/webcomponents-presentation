export class BaseElement extends HTMLElement {
    // ...
    adoptedCallback() {
        // The custom element has been moved into a new document
        // (e.g. someone called document.adoptNode(el)).
    }
    // ...
}
