export class BaseElement extends HTMLElement {
    // ...
    disconnectedCallback() {
        // Called when the element is removed from a document
    }
    // ...
}
