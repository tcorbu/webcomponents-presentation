export class BaseElement extends HTMLElement {
  //...
  connectedCallback() {
    // Called when the element is inserted into a document,
    // including into a shadow tree
  }
  //...
}
