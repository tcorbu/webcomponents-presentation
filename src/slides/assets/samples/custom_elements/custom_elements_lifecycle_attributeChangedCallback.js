export class BaseElement extends HTMLElement {
  //...
  static get observedAttributes() {
    // Monitor the 'version' attribute for changes.
    return ['version'];
  }

  attributeChangedCallback(attributeName, oldValue, newValue, namespace) {
      // Called when an attribute is changed, appended, removed,
      // or replaced on the element.
      // Only called for observed attributes.
  }
  //..
}

// Register the new element with the browser
window.customElements.define('tc-base', BaseElement);
