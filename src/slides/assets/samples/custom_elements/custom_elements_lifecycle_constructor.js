export class BaseElement extends HTMLElement {
    // ...
    constructor() {
        // Called when an instance of the element is created or upgraded.
        super();
    }
    // ...
}

// the constructor is not called here
window.customElements.define('tc-base', BaseElement);
