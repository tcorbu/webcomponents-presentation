const template = document.querySelector('template');
const header = template.content.querySelector('header h2');
header.textContent = 'Sandals';
//...
const clone = document.importNode(template.content, true);
document.body.appendChild(clone);
