import * as Reveal from 'reveal.js';

import {REVEAL_CONFIGURATION} from './RevealConfiguration';

export class RevealBootstraper {

    constructor() {

    }

    public static initialize() {
        Reveal.initialize(REVEAL_CONFIGURATION);
    }
}
