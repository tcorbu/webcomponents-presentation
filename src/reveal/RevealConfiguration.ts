import * as Reveal from 'reveal.js';

export const REVEAL_CONFIGURATION = {
    // Display controls in the bottom right corner
    controls: false,
    // Display a presentation progress bar
    progress: false,
    // If true; each slide will be pushed to the browser history
    history: true,
    // Loops the presentation, defaults to false
    loop: false,
    // Flags if mouse wheel navigation should be enabled
    mouseWheel: false,
    // Apply a 3D roll to links on hover
    rollingLinks: false,
    // UI style
    theme: Reveal.getQueryHash().theme || 'default',
    // Transition style
    transition: 'zoom',
    // Optional libraries used to extend on reveal.js
    // Options which are passed into marked
    // See https://github.com/chjj/marked#options-1
    markdown: {
        smartypants: false
    },
    dependencies: [
        // // Interpret Markdown in <section> elements
        // {
        //     src: REVEAL_LIB_PARTIAL_URL + 'plugin/markdown/marked.js', condition: function () {
        //     return !!document.querySelector('[data-markdown]');
        // }
        // },
        // {
        //     src: REVEAL_LIB_PARTIAL_URL + 'plugin/markdown/markdown.js', condition: function () {
        //     return !!document.querySelector('[data-markdown]');
        // }
        // },
        //
        //
        // // Syntax highlight for <code> elements
        // {
        //     src: REVEAL_LIB_PARTIAL_URL + 'plugin/highlight/highlight.js', async: true, callback: function () {
        //     hljs.initHighlightingOnLoad();
        //
        //     const list = document.querySelectorAll('pre code');
        //     for (let i = 0; i < list.length; i++) {
        //         hljs.highlightBlock(list[i]);
        //     }
        //
        // }
        // },
        //
        // // Zoom in and out with Alt+click
        // {src: REVEAL_LIB_PARTIAL_URL + 'plugin/zoom-js/zoom.js', async: true},
        //
        // // Speaker notes
        // {src: REVEAL_LIB_PARTIAL_URL + 'plugin/notes/notes.js', async: true},
        //
        // // MathJax
        // {src: REVEAL_LIB_PARTIAL_URL + 'plugin/math/math.js', async: true}

    ]
};
