export declare const REVEAL_CONFIGURATION: {
    controls: boolean;
    progress: boolean;
    history: boolean;
    loop: boolean;
    mouseWheel: boolean;
    rollingLinks: boolean;
    theme: any;
    transition: string;
    markdown: {
        smartypants: boolean;
    };
    dependencies: any[];
};
