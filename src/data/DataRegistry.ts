const requireContext = require.context('../../data/', true, /\.json$/);
export const data = {};
requireContext.keys().map((item: string) => {
    const value = requireContext(item);
    const entry:string = item.replace('./', '').split('\.', 1)[0];
    console.log(entry, value);
    data[entry] = value;
});

