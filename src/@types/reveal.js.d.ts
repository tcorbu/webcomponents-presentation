declare type  QueryParams = {theme: string}

declare class Reveal {

    static initialize(configure): Reveal

    static getQueryHash(): QueryParams
}