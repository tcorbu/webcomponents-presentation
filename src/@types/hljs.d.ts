declare type HighLightJSType = {
    initHighlightingOnLoad: Function,
    highlightBlock: (HTMLElement) => void
}

declare const hljs : HighLightJSType;