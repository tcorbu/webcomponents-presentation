'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const SvgStorePlugin = require('external-svg-sprite-loader/lib/SvgStorePlugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
var OUTPUT_PATH = path.join(__dirname, 'dist');
var SOURCE_PATH = path.join(__dirname, 'src');
var x =
    module.exports = [
        {
            context: SOURCE_PATH,
            resolve: {
                extensions: ['.ts', '.js', '.json']
            },
            entry: function () {
                return ['./main.ts', './style.scss']
            },
            output: {
                path: OUTPUT_PATH,
                filename: '[name].js'
            },
            module: {
                rules: [
                    {
                        test: /\.ts$/,
                        loaders: ['awesome-typescript-loader']
                    },
                    {
                        test: /\.scss$/,
                        loader: ExtractTextPlugin.extract({
                            fallback: 'style-loader',
                            use: 'css-loader!resolve-url-loader!sass-loader?sourceMap'
                        })
                    },
                    {
                        test: /\.jp[e]?g$/,
                        use: ["url-loader?mimetype=image/jpg"]
                    },
                    {
                        test: /\.png$/,
                        use: ["url-loader?mimetype=image/png"]
                    },
                    {
                        test: /\.md$/,
                        use: [
                            {
                                loader: "html-loader"
                            },
                            {
                                loader: "markdown-loader",
                                options: {
                                    /* your options here */
                                }
                            }
                        ]
                    },
                    {
                        test: /\.pug/,
                        use: [
                            {
                                loader: "pug-loader",
                                options: {
                                    pretty: true
                                }
                            }]

                    }, {
                        test: /\.svg/,
                        use: [
                            {
                                loader: 'external-svg-sprite-loader',
                                options: {
                                    bundle: 'icons-bundle.svg',
                                    referencePrefix: '',
                                    referenceSuffix: '',
                                    symbolIdGenerator: 'fileName' // or hash
                                }
                            }
                        ]
                    }
                ]
            },
            plugins: [
                new SvgStorePlugin(),
                new ExtractTextPlugin({
                    filename: '[name].css'
                }),
                new HtmlWebpackPlugin({
                    template: './main.pug',
                    filename: 'index.html',
                    inject: true
                })
            ]
        }
    ];