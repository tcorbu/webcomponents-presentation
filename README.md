# Webcomponents - slideshow

A presentation that showcases the new WebComponents, the native ones.

This guides the attendees to discover what is good and bad about them as well as
learn some tricks in HTML.

The slides contains :

    01 introduction
    02 history
    